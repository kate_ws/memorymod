package memory.memory.data.enums;

public enum MemoryType {
    L1, L2
}
