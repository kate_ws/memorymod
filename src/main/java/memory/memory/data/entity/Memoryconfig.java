package memory.memory.data.entity;

import memory.memory.data.enums.MemoryType;

import javax.persistence.*;

@Entity
@Table(name = "memoryconfig")
public class Memoryconfig {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name = "type")
    private MemoryType memoryType;

    @Column(name = "sizeFrom")
    private Long sizeFrom;

    @Column(name = "sizeTo")
    private Long sizeTo;

    public Long getId() {
        return this.id;
    }
    public MemoryType getMemoryType() {
        return this.memoryType;
    }
    public Long getSizeFrom() {
        return this.sizeFrom;
    }
    public Long getSizeTo() {
        return this.sizeTo;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setMemoryType(MemoryType memoryType) {
        this.memoryType = memoryType;
    }
    public void setSizeFrom(Long size) {
        this.sizeFrom = size;
    }
    public void setSizeTo(Long size) {
        this.sizeTo = size;
    }
}
