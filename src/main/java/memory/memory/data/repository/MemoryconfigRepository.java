package memory.memory.data.repository;

import memory.memory.data.entity.Memoryconfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemoryconfigRepository extends JpaRepository<Memoryconfig, Long> {

    Memoryconfig getBySizeFrom(Long size);

    Memoryconfig getBySizeTo(Long size);

}
