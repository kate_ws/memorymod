package memory.memory.redisstorage.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.File;
import java.util.Objects;

@RedisHash("DataStorage")
public class DataStorage {

    @Id
    private String id;

    private File file;


    public String getId() {
        return this.id;
    }

    public File getFile() {
        return this.file;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, file);
    }

    public String toString() {
        return "DataStorage(id=" + id +
                ", file=" + file +
                ")";
    }
}
