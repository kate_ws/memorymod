package memory.memory.redisstorage.repository;

import memory.memory.redisstorage.entity.DataStorage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataStorageRepository extends CrudRepository<DataStorage, String> {
}
