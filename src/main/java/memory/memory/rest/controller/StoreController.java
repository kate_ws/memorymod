package memory.memory.rest.controller;

import memory.memory.rest.model.dto.response.RestResponseEntity;
import memory.memory.services.api.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/store-domain", produces = MediaType.APPLICATION_JSON_VALUE)
public class StoreController {

    private StoreService storeService;

    @Autowired
    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping(value = "get-folder-content")
    public RestResponseEntity getElements() {
        return storeService.getFolder();
    }

    @GetMapping(value = "store-element")
    public RestResponseEntity storeElement(@RequestParam String elementName) {
        return storeService.storeElement(elementName);
    }

}
