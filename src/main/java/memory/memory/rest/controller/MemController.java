package memory.memory.rest.controller;

import memory.memory.rest.model.dto.request.MemoryConfigRequestDto;
import memory.memory.rest.model.dto.response.RestResponseEntity;
import memory.memory.services.api.MemoryConfgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/mem", produces = MediaType.APPLICATION_JSON_VALUE)
public class MemController {

    private MemoryConfgService memoryConfgService;

    @Autowired
    public MemController(MemoryConfgService memoryConfgService) {
        this.memoryConfgService = memoryConfgService;
    }


    @PostMapping(value = "set")
    public RestResponseEntity set(
            @RequestBody MemoryConfigRequestDto memoryConfigRequestDto) {
        return memoryConfgService.addConfig(memoryConfigRequestDto);
    }
}
