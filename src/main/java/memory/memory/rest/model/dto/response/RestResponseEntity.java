package memory.memory.rest.model.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class RestResponseEntity implements Serializable {

  private static final long serialVersionUID = -3530663544234598061L;

  private Boolean success;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Object data;

  private Long timestamp;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String message;

  public RestResponseEntity() {
    this.success = true;
    setTimestamp();
  }
  public RestResponseEntity(Object data) {
    this();
    this.data = data;
  }
  public RestResponseEntity(Object data, String message) {
    this(data);
    this.message = message;
  }
  private void setTimestamp() {
    this.timestamp = Instant.now().toEpochMilli();
  }
  public void setFailure(String message) {
    this.success = false;
    this.message = message;
    setTimestamp();
  }
  public Boolean getSuccess() {
    return this.success;
  }
  public Object getData() {
    return this.data;
  }
  public Long getTimestamp() {
    return this.timestamp;
  }
  public String getMessage() {
    return this.message;
  }
  public void setSuccess(Boolean success) {
    this.success = success;
  }
  public void setData(Object data) {
    this.data = data;
  }
  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }
  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof RestResponseEntity)) return false;
    RestResponseEntity restResponseEntity = (RestResponseEntity) o;
    return success.equals(restResponseEntity.success) &&
            Objects.equals(data, restResponseEntity.data) &&
            Objects.equals(timestamp, restResponseEntity.timestamp) &&
            Objects.equals(message, restResponseEntity.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(success, data, timestamp, message);
  }

  public String toString() {
    return "RestResponseEntity(success=" + getSuccess() +
            ", data=" + getData() +
            ", timestamp=" + getTimestamp() +
            ", message=" + getMessage() +
            ")";
  }
}
