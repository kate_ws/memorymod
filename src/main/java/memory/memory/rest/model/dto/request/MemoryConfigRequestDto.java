package memory.memory.rest.model.dto.request;

import memory.memory.data.enums.MemoryType;

import java.math.BigDecimal;

public class MemoryConfigRequestDto {

    public Long sizeFrom;

    public Long sizeTo;

    public MemoryType memoryType;

    public Long getSizeFrom() {
        return this.sizeFrom;
    }
    public Long getSizeTo() {
        return this.sizeTo;
    }
    public MemoryType getMemoryType() {
        return this.memoryType;
    }
    public void setSizeFrom(Long sizeFrom) {
        this.sizeFrom = sizeFrom;
    }
    public void setSizeTo(Long sizeFrom) {
        this.sizeTo = sizeFrom;
    }
    public void setMemoryType(MemoryType memoryType) {
        this.memoryType = memoryType;
    }
}
