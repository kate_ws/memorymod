package memory.memory.services.impl;

import memory.memory.data.entity.Memoryconfig;
import memory.memory.data.repository.MemoryconfigRepository;
import memory.memory.rest.model.dto.request.MemoryConfigRequestDto;
import memory.memory.rest.model.dto.response.RestResponseEntity;
import memory.memory.services.api.MemoryConfgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class MemoryConfgServiceImpl implements MemoryConfgService {

    private MemoryconfigRepository memoryconfigRepository;

    @Autowired
    public MemoryConfgServiceImpl(MemoryconfigRepository memoryconfigRepository) {
        this.memoryconfigRepository = memoryconfigRepository;
    }

    @Override
    public RestResponseEntity addConfig(MemoryConfigRequestDto configRequestDto) {
        Memoryconfig entity = new Memoryconfig();
        entity.setMemoryType(configRequestDto.getMemoryType());
        entity.setSizeFrom(configRequestDto.getSizeFrom());
        entity.setSizeTo(configRequestDto.getSizeTo());
        if (Objects.isNull(memoryconfigRepository.getBySizeFrom(configRequestDto.getSizeFrom()))) {
            if (Objects.isNull(memoryconfigRepository.getBySizeTo(configRequestDto.getSizeTo()))) {
                return new RestResponseEntity(memoryconfigRepository.save(entity));
            }
        }
        return new RestResponseEntity("already exists");
    }



}
