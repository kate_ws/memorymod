package memory.memory.services.impl;

import memory.memory.data.entity.Memoryconfig;
import memory.memory.data.enums.MemoryType;
import memory.memory.data.repository.MemoryconfigRepository;
import memory.memory.redisstorage.entity.DataStorage;
import memory.memory.redisstorage.repository.DataStorageRepository;
import memory.memory.rest.model.dto.response.RestResponseEntity;
import memory.memory.services.api.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {

    @Value("${memory.default}")
    private String defaultType;

    @Value("${memory.storage-path}")
    private String storagePath;

    @Value("${memory.L2-path}")
    private String L2Path;


    private MemoryconfigRepository memoryconfigRepository;

    private DataStorageRepository dataStorageRepository;

    @Autowired
    public StoreServiceImpl(MemoryconfigRepository memoryconfigRepository, DataStorageRepository dataStorageRepository) {
        this.memoryconfigRepository = memoryconfigRepository;
        this.dataStorageRepository = dataStorageRepository;
    }


    @Override
    public RestResponseEntity getFolder() {
        File folder = new File(storagePath);
        File[] listOfFiles = folder.listFiles();

        List<String> fileList = new ArrayList<>();
        for (File file : listOfFiles) {
            fileList.add(file.getName());
        }
        return new RestResponseEntity(fileList);
    }

    @Override
    public RestResponseEntity storeElement(String element) {
        List<Memoryconfig> memoryconfigs = memoryconfigRepository.findAll();
        File file = new File(storagePath+element);
        for (Memoryconfig memoryconfig : memoryconfigs) {
            if (file.length() > memoryconfig.getSizeFrom() && file.length() < memoryconfig.getSizeTo()) {
                if (MemoryType.L2.equals(memoryconfig.getMemoryType())) {
                    Path filePath = Paths.get(L2Path+element);

                    return new RestResponseEntity("wrote to L2");
                } else if (MemoryType.L1.equals(memoryconfig.getMemoryType())) {
                    DataStorage ds = new DataStorage();
                    ds.setFile(file);
                    ds.setId(storagePath+element);
                    dataStorageRepository.save(ds);
                    return new RestResponseEntity("wrote to L1");
                }
            }
        }
        if (MemoryType.L2.equals(defaultType)) {
            Path filePath = Paths.get(L2Path+element);

            return new RestResponseEntity("wrote to L2");
        } else if (MemoryType.L1.equals(defaultType)) {
            DataStorage ds = new DataStorage();
            ds.setFile(file);
            ds.setId(storagePath+element);
            dataStorageRepository.save(ds);
            return new RestResponseEntity("wrote to L1");
        }
        return new RestResponseEntity("nothing to wrote");
    }



}
