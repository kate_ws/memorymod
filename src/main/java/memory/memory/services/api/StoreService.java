package memory.memory.services.api;

import memory.memory.rest.model.dto.response.RestResponseEntity;

public interface StoreService {
    RestResponseEntity getFolder();

    RestResponseEntity storeElement(String element);
}
