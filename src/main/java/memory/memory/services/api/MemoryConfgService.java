package memory.memory.services.api;

import memory.memory.rest.model.dto.request.MemoryConfigRequestDto;
import memory.memory.rest.model.dto.response.RestResponseEntity;

public interface MemoryConfgService {
    RestResponseEntity addConfig(MemoryConfigRequestDto configRequestDto);
}
